import 'package:flutter/material.dart';
import 'package:hello/hello.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Center(
          child: Column(
            children: [
              GLTiles(
                title: '123',
              ),
              GLTiles(
                title: '456',
              ),
              GLTiles(
                title: '123',
              ),
              GLTiles(
                title: '456',
              ),
              GLTiles(
                title: '789',
              ),
            ],
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';

BoxDecoration get borderDecoration => BoxDecoration(
    borderRadius: BorderRadius.all(Radius.circular(10)),
    border: Border.all(color: Color(0xffCEDCFD), width: 1.5));

// ignore: must_be_immutable
class GLTiles extends StatefulWidget {
  /// 左边的图标
  Widget leading;

  /// 主标题
  String title;

  /// title 的颜色
  Color titleColor;

  /// 背景 的颜色
  Color backgroundColor;

  /// title的字体大小
  double titleSize;

  /// 子标题
  String subtitle;

  /// 子操作栏
  Widget subTrailing;

  /// 操作栏，默认是一个向右的箭头，
  /// 如果不想要箭头，就传个 SizedBox() 进来
  Widget trailing;

  /// 左侧图标的高度
  double iconHeight;

  // listtile的高度
  double height;

  // 是否选中
  bool isSelected;

  // 是否需要check
  bool ischeck;

  // 是否需要显示方形边框，该参数只在 ischeck 为 false 时有效
  bool isborder;

  // 是否显示分割线
  bool isdivider;

  // indent 分割线的长度，默认是撑满一行
  double indent;

  // callback回调
  VoidCallback onTap;

  // 圆角
  double radius;

  // 禁用
  bool enable;

  // 主标题风格
  TextStyle textStyle;

  GLTiles(
      {Key key,
      this.leading,
      this.title,
      this.trailing,
      this.subTrailing,
      this.titleColor,
      this.backgroundColor,
      this.height,
      this.iconHeight,
      this.subtitle,
      this.titleSize,
      this.isdivider = true,
      this.isSelected = false,
      this.ischeck = false,
      this.isborder = true,
      this.indent = 0,
      this.radius = 0,
      this.enable = false,
      this.textStyle,
      this.onTap})
      : super(key: key);

  @override
  _GLTileState createState() => _GLTileState();
}

class _GLTileState extends State<GLTiles> {
  Widget get _iconContainer => Container(
      margin: EdgeInsets.only(right: 13),
      decoration: widget.isborder ? borderDecoration : null,
      width: widget.iconHeight ?? 36,
      height: widget.iconHeight ?? 36,
      child: widget.leading);

  Widget get _titleContainer => Container(
        child: Text(
          widget.title ?? '',
          overflow: TextOverflow.ellipsis,
          softWrap: true,
          maxLines: 1,
          style: widget.textStyle ??
              TextStyle(
                  color: widget.titleColor ?? Color(0xff4A5091),
                  fontSize: widget.titleSize ?? 15),
        ),
      );

  Widget get _subTitleContainer => Text(widget.subtitle,
      style: TextStyle(
        fontSize: 12,
        color: Color(0xff989898),
      ));
  Widget get _subtrailing => Container(
        child: Container(child: widget.subTrailing),
      );

  /// [分割线]
  static Widget _divider({double ident = 0}) => Divider(
        height: 0,
        color: Color(0xffCEDCFD),
        thickness: 0,
        indent: ident,
      );
  @override
  Widget build(BuildContext context) {
    var onTap = widget.enable ? () {} : widget.onTap ?? () {};
    var leading = widget.leading != null ? _iconContainer : SizedBox();
    if (widget.ischeck) {
      widget.height = widget.height ?? 44;
      leading = Opacity(
        opacity: widget.isSelected ? 1 : 0,
        child: Padding(
          padding: EdgeInsets.only(right: 10),
          child: Icon(
            Icons.check,
            size: 20,
            color: Color(0xff1acce0),
          ),
        ),
      );
    }
    var isdivider =
        widget.isdivider ? _divider(ident: widget.indent) : SizedBox();
    var backgroundColor = widget.enable
        ? Color.fromRGBO(0, 0, 0, 0.1)
        : widget.backgroundColor ?? Color(0xffffffff);

    var mainContentContainer = widget.subtitle != null
        ? Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              _titleContainer,
              _subTitleContainer,
            ],
          )
        : _titleContainer;
    var trailingContainer =
        widget.trailing ?? Icon(Icons.chevron_right, color: Color(0xff989898));
    var subTrailingContainer =
        widget.subTrailing != null ? _subtrailing : SizedBox();
    return Container(
        child: Column(
      children: <Widget>[
        GestureDetector(
          onTap: onTap,
          child: Container(
            decoration: BoxDecoration(
                color: backgroundColor,
                borderRadius: BorderRadius.all(Radius.circular(widget.radius))),
            height: widget.height ?? 62,
            padding: EdgeInsets.symmetric(horizontal: 17),
            child: Flex(
              direction: Axis.horizontal,
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Container(
                    child: Row(
                      children: <Widget>[
                        leading,
                        Expanded(
                          child: mainContentContainer,
                        )
                      ],
                    ),
                  ),
                ),
                subTrailingContainer,
                SizedBox(
                  width: 13,
                ),
                trailingContainer,
              ],
            ),
          ),
        ),
        isdivider
      ],
    ));
  }
}
